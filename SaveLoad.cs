﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveLoad
{
    public static void Save_Level(int levelNum)
    {
        PlayerPrefs.SetInt("Level", levelNum);
    }

    public static int Load_Level()
    {
        if (PlayerPrefs.HasKey("Level"))
            return PlayerPrefs.GetInt("Level");
        else
            return -1;
    }

    public static void Save_LettersPerSecond(float letters)
    {
        PlayerPrefs.SetFloat("Letters", letters);
    }

    public static float Load_LettersPerSecond()
    {
        if (PlayerPrefs.HasKey("Letters"))
            return PlayerPrefs.GetFloat("Letters");
        else
            return 0;
    }

    public static void Save_HasPlayed()
    {
        PlayerPrefs.SetInt("Played", 1);
    }

    public static int Load_HasPlayed()
    {
        if (PlayerPrefs.HasKey("Played"))
            return PlayerPrefs.GetInt("Played");
        else
            return 0;
    }
	
}
