﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.UI;

public class AI_Enemy : MonoBehaviour {

    public enum AI_STATE { Idle, Chase, Attack, Dead };

    //References
    Animator anim;
    NavMeshAgent agent;
    public Transform playerTrans;
    UI_Score uiScore;
    protected Health playerHealth = null;
    protected AudioSource attackSource;

    public static int activeEnemies = 0;
    public Transform wpPos = null;

    public Text currWordText; //equivalent of the health bar, assign in inspector
    public int pointWorth;
    public string currWord;
    public int attackDmg = 10;

    public bool isSneaky = false;
    public float sneakyDistance = 2f;

    [Space]
    [SerializeField]
    AI_STATE currState = AI_STATE.Idle;
    /// <summary>
    /// How much of the current word matches the typed word
    /// </summary>
    public string matchedWord; //How much of the current word matches the typed word

    //Events
    public UnityEvent OnStateChanged;
    public UnityEvent OnIdleEnter;
    public UnityEvent OnChaseEnter;
    public UnityEvent OnAttackEnter;
    public UnityEvent OnTypingChanged;
    public UnityEvent OnTypingMatched;

    public AI_STATE ActiveState
    {
        get { return currState; }
        set
        {
            StopAllCoroutines();
            currState = value;

            //Determine the state
            switch (currState)
            {
                case AI_STATE.Idle:
                    StartCoroutine(StateIdle());
                    break;
                case AI_STATE.Chase:
                    StartCoroutine(StateChase());
                    break;
                case AI_STATE.Attack:
                    StartCoroutine(StateAttack());
                    break;
                case AI_STATE.Dead:
                    StartCoroutine(StateDead());
                    break;
            }

            OnStateChanged.Invoke();
        }
    }

    void Awake()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        playerTrans = GameObject.FindWithTag("PlayerChar").transform;
        Debug.Log("Player trans: " + playerTrans.gameObject.name + " pos: " + playerTrans.position);
        playerHealth = playerTrans.GetComponent<Health>();

        //Debug.Log(playerHealth.gameObject.name);
        //UI Stuff
        attackSource = GetComponent<AudioSource>();
        uiScore = GameObject.Find("Txt_Score").GetComponent<UI_Score>();

        //Hide ui
        //currWordText.gameObject.SetActive(false);

        if (isSneaky)
        {
            currWordText.gameObject.SetActive(false);
            if (sneakyDistance < agent.stoppingDistance+1)
                Debug.LogWarning(this.gameObject.name + "'s sneaky distance is less than his stopping distance, the healthbar will never appear!");
        }
    }

    void Update()
    {
        if(isSneaky && currState == AI_STATE.Chase)
        {
            //Debug.Log("Test, Dis: " + Vector3.Distance(playerTrans.position, transform.position));
            if(Vector3.Distance(playerTrans.position, transform.position) < sneakyDistance)
            {
                currWordText.gameObject.SetActive(true);
            }
        }
    }
    void Start()
    {
        ActiveState = currState;
        currWord = WordList.Instance.RandomWord();
        _UpdateText();
    }

    public void _UpdateText()
    {

        //Build string for UI
        if (matchedWord != null)
        {
            //Debug.Log("Matched length: " + matchedWord.Length);
            //Debug.Log("Curr length: " + currWord.Length);
            currWordText.text = "<color=red>" + matchedWord + "</color>" + currWord.Substring(matchedWord.Length, currWord.Length - matchedWord.Length);

        }
        else
            currWordText.text = currWord;
    }

    public void _UpdateTypedWord()
    {
        if (ActiveState != AI_STATE.Chase && ActiveState != AI_STATE.Attack) return;

        matchedWord = WordList.CompareWords(Typing.typedWord, currWord);
        //Debug.Log("Matched: " + matchedWord);
        if (matchedWord.Length != currWord.Length)
            return;

        //Debug.Log("We matched!");
        if (matchedWord.Equals(currWord))
            OnTypingMatched.Invoke();


    }

    public virtual void DealDamage()
    {
        playerHealth.Amount -= attackDmg;
        attackSource.Play();

    }

    public void _WakeUp(Transform wpPos)
    {
        this.wpPos = wpPos;

        ActiveState = AI_STATE.Chase;
    }

    public void _Die()
    {
        agent.isStopped = true;

        //Debug.Log("Increased score");
        GameManager.Instance.score += pointWorth;
        uiScore.OnScoreChange.Invoke();
        currWordText.gameObject.SetActive(false);

        float lettersPerSecond = currWord.Length / Typing.elapsedTime;

        if (lettersPerSecond > Typing.personalLettersPerSecond)
            Typing.personalLettersPerSecond = lettersPerSecond;

        if (lettersPerSecond < Typing.recordLettersPerSecond)
            ++GameManager.Instance.bonusLevel;

        ActiveState = AI_STATE.Dead;
        --activeEnemies;
        matchedWord = string.Empty;
        Player_Nav.Instance.enemyDie.Invoke();
        
    }

    public void _Reset()
    {
        matchedWord = string.Empty;
        currWord = WordList.Instance.RandomWord();
        _UpdateText();
    }

    #region states
    public IEnumerator StateIdle()
    {
        //Run anim
        anim.SetInteger("Anim State", (int)ActiveState);
        while (ActiveState == AI_STATE.Idle)
            yield return null;
    }

    public IEnumerator StateChase()
    {
        ++activeEnemies;

        //Run anim
        anim.SetInteger("Anim State", (int)ActiveState);

        if (wpPos == null)
        {
            //Set destination
            agent.SetDestination(playerTrans.position);
            Debug.Log("player pos: " + playerTrans.position);
        }
        else
        {
            //Set destination
            agent.SetDestination(wpPos.position);
            Debug.Log("wp pos: " + wpPos.position);
        }
        //Wait until path is calc
        while (!agent.hasPath)
            yield return null;

        while(ActiveState == AI_STATE.Chase)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                //agent.Stop();
                agent.isStopped = true;
                yield return null;
                //wait a frame, avoids bugs
                ActiveState = AI_STATE.Attack;
                yield break;
            }

            yield return null;
        }
    }

    public IEnumerator StateAttack()
    {
        anim.SetInteger("Anim State", (int)ActiveState);

        while (ActiveState == AI_STATE.Attack)
        {
            //Look at player
            Vector3 lookPos = new Vector3(playerTrans.position.x, transform.position.y, playerTrans.position.z);
            transform.LookAt(lookPos, transform.up);

            //Chase if we too far
            float distance = Vector3.Distance(playerTrans.position, transform.position);
            if(distance > agent.stoppingDistance * 2f)
            {
                agent.isStopped = false;
                yield return null;
                ActiveState = AI_STATE.Chase;
                yield break;
            }

            yield return null;
        }
    }

    public IEnumerator StateDead()
    {
        anim.SetInteger("Anim State", (int)ActiveState);
        bool did = false;
        while (ActiveState == AI_STATE.Dead)
        {
            if (!did && anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
            {
                anim.SetInteger("Anim State", 4);
                did = true;
            }
            else
            {
                yield return null;
            }
        }
            
    }
    #endregion
}
