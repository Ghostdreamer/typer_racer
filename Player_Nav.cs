﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class Player_Nav : MonoBehaviour
{
    /// <summary>
    /// Current node the camera is at
    /// </summary>
    public int currNode = 0;    //Camera pos

    //References
    Button navButton;           //Ref. to nav button
    Animator anim;

    //Enforce singleton, with "smart" creation 
    static Player_Nav instance;
    public static Player_Nav Instance
    {
        get
        {
            //Get or create
            if(instance == null)
            {
                GameObject GO = new GameObject("Navigator");
                instance = GO.AddComponent<Player_Nav>();
            }

            return instance;
        }
        set
        {
            if(instance != null)
            {
                if(instance.GetInstanceID() != value.GetInstanceID())
                {
                    DestroyImmediate(value.gameObject);
                    return;
                }
            }

            instance = value;
        }
    }

    int animStateHash = Animator.StringToHash("Node Number");

    //Events
    public UnityEvent enemyDie;

    void Awake()
    {
        Instance = this;
        anim = GetComponent<Animator>();
        navButton = GameObject.FindGameObjectWithTag("NavBtn").GetComponent<Button>();
        navButton.gameObject.SetActive(false);
    }

    public void _Next()
    {
        Typing.typedWord = string.Empty;
        ++currNode;
        anim.SetInteger(animStateHash, currNode);
    }

    public void _Previous()
    {
        Typing.typedWord = string.Empty;
        ++currNode;
        anim.SetInteger(animStateHash, currNode);
    }

    public void ShowMoveBtn()
    {
        //Debug.Log(AI_Enemy.activeEnemies);


        if (AI_Enemy.activeEnemies > 0) return;

        navButton.gameObject.SetActive(true);
        navButton.Select();
        
    }
}
