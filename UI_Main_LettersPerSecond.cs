﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_Main_LettersPerSecond : MonoBehaviour {
    public Text LPS;
    public bool reset = false;
	// Use this for initialization
	void Start () {
        if(reset)
        {
            PlayerPrefs.DeleteAll();
        }

        LPS.text = SaveLoad.Load_LettersPerSecond().ToString("0.##");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
