﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Vector3 moveDir;
    public Health playerHealth;
    public int attackDmg;
    public AudioSource attackSource;

    public Transform aimPos;
    void Update()   
    {
        moveDir = (aimPos.position - transform.position).normalized;

        transform.Translate(moveDir * 5f * Time.deltaTime);
    }

	void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hit!");
        if(other.gameObject.CompareTag("Player"))
        {
            playerHealth.Amount -= attackDmg;
            attackSource.Play();
        }
    }
}
