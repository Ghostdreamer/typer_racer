﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Bonus : MonoBehaviour {

    public GameObject[] bonusObjs;

    void Awake()
    {
        bonusObjs = GameObject.FindGameObjectsWithTag("BonusObj");
    }

    void Update()
    {
        for(int i = 0; i < bonusObjs.Length; i++)
        {
            if (i < GameManager.Instance.bonusLevel)
                bonusObjs[i].SetActive(true);
            else
                bonusObjs[i].SetActive(false);
        }
    }
}
