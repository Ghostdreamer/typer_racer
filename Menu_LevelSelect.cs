﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_LevelSelect : MonoBehaviour {

    public Button[] levels;

	void Start()
    {

        int highestLevel = SaveLoad.Load_Level();
        highestLevel++;

        for(int i = 0; i < levels.Length; i++)
        {
            levels[i].interactable = (i <= highestLevel);
        }
    }
}
