﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordList : MonoBehaviour {

    //Singleton
    static WordList instance;
    public static WordList Instance
    {
        get
        {
            //Get or create
            if (instance == null)
            {
                GameObject GO = new GameObject("WordList");
                instance = GO.AddComponent<WordList>();
            }

            return instance;
        }
        set
        {
            if (instance != null)
            {
                if (instance.GetInstanceID() != value.GetInstanceID())
                {
                    DestroyImmediate(value.gameObject);
                    return;
                }
            }

            instance = value;
            DontDestroyOnLoad(instance.gameObject);
        }
    }

    public TextAsset wordListFile;
    string[] words;

    //TODO: add user word list as well
    void Awake()
    {
        Instance = this;

        //Load word list
        if(wordListFile == null)
        {
            wordListFile = (TextAsset)Resources.Load("English_WordList");
            //Split on Regex newline
            words = wordListFile.text.Split(new[] { "\r\n" }, System.StringSplitOptions.None);
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <returns>A random word from the word list</returns>
    public string RandomWord()
    {
        //Safeguard against any casing issues
        return words[Random.Range(0, words.Length)].ToLower();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="wordLength">Maximum length of the word</param>
    /// <returns>A random word from the word list</returns>
    public string RandomWord(int wordLength)
    {
        string word = "";

        while(word.Length > wordLength)
        {
            word = RandomWord();
        }

        return word;
    }

    /// <summary>
    /// Compares two strings and returns how much matches
    /// </summary>
    /// <example>word1 = "hello", word2 = "helicopter", return = "hel"</example>
    /// <param name="word1"></param>
    /// <param name="word2"></param>
    /// <returns>The extent of the match</returns>
    public static string CompareWords(string word1, string word2)
    {
        string match = "";

        //only compare the shortest, fixes null error
        int shortestLength = Mathf.Min(word1.Length, word2.Length);

        for(int i = 0; i < shortestLength; i++)
        {
            if (word1[i] != word2[i]) //at this point we no longer match, return
                return match;

            match += word1[i]; //both letters are the same, add it to the match
        }

        return match; //both words are the same up to the point of the shorts word, return it
    }
}
