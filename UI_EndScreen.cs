﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EndScreen : MonoBehaviour {

    public Text recordText;
    public GameObject typing;
    public int levelNumber;
	void OnEnable ()
    {
        AI_Enemy.activeEnemies = 0;

        Debug.Log("Enabled");
        recordText.text = "Record Letters Per Second: " + Typing.personalLettersPerSecond.ToString("0.##"); 

        if (SaveLoad.Load_LettersPerSecond() < Typing.personalLettersPerSecond)
            SaveLoad.Save_LettersPerSecond(Typing.personalLettersPerSecond);

        if (SaveLoad.Load_Level() < levelNumber)
            SaveLoad.Save_Level(levelNumber);

        typing.SetActive(false);
    }
}
