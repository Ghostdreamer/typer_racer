﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Typing : MonoBehaviour {
    /// <summary>
    /// The word that has been typed by the player
    /// </summary>
    public static string typedWord = string.Empty;

    public static float elapsedTime = 0.0f;//since last reset
    public static float recordLettersPerSecond = 2f;
    public static float personalLettersPerSecond = 0f;

    public UnityEvent OnTypingChanged;  //what to do when typing has happened
    public AudioClip[] combatSounds;    //sounds for combat



    AI_Enemy[] enemies; //All the enemies
    Text typedText = null;  //text object for showing letters typed
    AudioSource source = null;


    void Awake()
    {
        //get all the enemies in the scene
        enemies = Object.FindObjectsOfType<AI_Enemy>();

        source = GetComponent<AudioSource>();
        source.clip = combatSounds[0];
        typedText = GetComponentInChildren<Text>();
    }

    void Update()
    {
        //Debug.Log("Length: " + Input.inputString.Length);
        elapsedTime += Time.deltaTime;

        //use the input manager and grab whatever input is happening this frame (if any)
        if (Input.inputString.Length > 0)
        {
            typedWord += Input.inputString.ToLower();
            UpdateTyping();
        }
    }

    //Call the event
    void UpdateTyping()
    {
        //tell all the enemies you are typing
        foreach(AI_Enemy enemy in enemies)
        {
            enemy.OnTypingChanged.Invoke();
        }

        //do whatever (update UI)
        OnTypingChanged.Invoke();

        //check for reset
        foreach(AI_Enemy enemy in enemies)
        {
            if (typedWord.Length - enemy.matchedWord.Length <= 0)
                return;
        }

        Reset();
    }

    public void Reset()
    {
        typedWord = string.Empty;
        elapsedTime = 0.0f;

        foreach(AI_Enemy enemy in enemies)
        {
            enemy.OnTypingChanged.Invoke();
        }
    }

    public void UpdateUIText()
    {
        typedText.text = Input.inputString;
        source.clip = combatSounds[Random.Range(0, combatSounds.Length)];
    }
}
