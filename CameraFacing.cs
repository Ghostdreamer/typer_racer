﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacing : MonoBehaviour {

	void LateUpdate()
    {
        transform.LookAt(Camera.main.transform);
    }
}
