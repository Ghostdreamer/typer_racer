﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_Typing : MonoBehaviour
{
    /// <summary>
    /// The word that has been typed by the player
    /// </summary>
    public static string typedWord = string.Empty;

    public UnityEvent OnTypingChanged;  //what to do when typing has happened

    Menu_Words[] menus;

    void Awake()
    {
        //typedText = GetComponentInChildren<Text>();
        menus = Object.FindObjectsOfType<Menu_Words>();

    }

    void Update()
    {
        //use the input manager and grab whatever input is happening this frame (if any)
        if (Input.inputString.Length > 0)
        {
            typedWord += Input.inputString.ToLower();
            Debug.Log("Typed word: " + typedWord);
            UpdateTyping();
        }
    }

    //Call the event
    void UpdateTyping()
    {
        //do whatever (update UI)
        OnTypingChanged.Invoke();

        foreach (Menu_Words menuW in menus)
        {
            if (typedWord.Length - menuW.matchedWord.Length <= 0)
                return;
        }

        Reset();
    }

    public void Reset()
    {
        typedWord = string.Empty;

        foreach (Menu_Words menuW in menus)
        {
            menuW.OnTypingChanged.Invoke();
        }
    }

    //public void UpdateUIText()
    //{
        //typedText.text = Input.inputString;
    //}

}
