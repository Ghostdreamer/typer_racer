﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Menu_Words : MonoBehaviour {

    public string currWord;
    public Text currWordText;

    /// <summary>
    /// How much of the current word matches the typed word
    /// </summary>
    public string matchedWord; //How much of the current word matches the typed word
    public UnityEvent OnTypingChanged;
    public UnityEvent OnTypingMatched;

    public void _UpdateText()
    {

        //Build string for UI
        if (matchedWord != null)
        {
            currWordText.text = "<color=#00BCD0FF>" + matchedWord + "</color>" + "<color=#007F8DFF>" + currWord.Substring(matchedWord.Length, currWord.Length - matchedWord.Length) + "</color>";
        }
        else
            currWordText.text = currWord;
    }

    public void _UpdateTypedWord()
    {
        matchedWord = WordList.CompareWords(UI_Typing.typedWord, currWord);
        if (matchedWord.Length != currWord.Length)
            return;

        if (matchedWord.Equals(currWord))
            OnTypingMatched.Invoke();


    }
}
