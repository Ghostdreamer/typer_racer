﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
public class GameTrigger : MonoBehaviour {

    public UnityEvent OnTriggerEntered;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("PlayerChar")) return;

        OnTriggerEntered.Invoke();
    }
}
