﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceVisibility : MonoBehaviour {

    public GameObject objToHide = null;

    void OnTriggerEnter(Collider col)
    {
        if (!col.CompareTag("Player"))
            return;
        if (objToHide != null)
            objToHide.SetActive(true);
    }

    void OnTriggerExit(Collider col)
    {
        if (!col.CompareTag("Player"))
            return;
        if (objToHide != null)
            objToHide.SetActive(false);
    }
}
