﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

	public float Amount
    {
        get { return amount; }
        set
        {
            amount = value;
            OnHealthChanged.Invoke();

            if (amount <= 0f)
                OnHealthExpired.Invoke();
        }
    }

    [SerializeField]
    [Range(0f, 100f)]
    float amount = 100f;

    //Events
    public UnityEvent OnHealthChanged;
    public UnityEvent OnHealthExpired;

    public void _TakeDamage(int amt)
    {
        Amount -= amt;
    }

}
