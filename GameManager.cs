﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Enforce singleton, with "smart" creation 
    static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            //Get or create
            if (instance == null)
            {
                GameObject GO = new GameObject("Game Manager");
                instance = GO.AddComponent<GameManager>();
            }

            return instance;
        }
        set
        {
            if (instance != null)
            {
                if (instance.GetInstanceID() != value.GetInstanceID())
                {
                    DestroyImmediate(value.gameObject);
                    return;
                }
            }

            instance = value;
        }
    }

    public float score;
    public int bonusLevel;

    void Awake()
    {
        Instance = this;
    }
}
