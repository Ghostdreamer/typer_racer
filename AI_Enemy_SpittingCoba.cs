﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Enemy_SpittingCoba : AI_Enemy {
    public GameObject poisonSpit;
    public Transform jaw;
    public Transform aimPos;

	public override void DealDamage()
    {
        GameObject go = Instantiate(poisonSpit, jaw.position, Quaternion.identity);
        Projectile proj = go.AddComponent <Projectile>();

        proj.attackDmg = attackDmg;
        proj.attackSource = attackSource;
        proj.playerHealth = playerHealth;
        proj.aimPos = aimPos;

        Destroy(proj.gameObject, 2f);
        
    }
}
