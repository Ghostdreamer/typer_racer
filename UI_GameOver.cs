﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GameOver : MonoBehaviour
{
    public GameObject fadeObj;
    public Animator anim;
    public Text completionText;
    public Text lettersPerSecondText;
    public GameObject nextLevelBtn;
    public GameObject restartLevelBtn;
    public GameObject typing;

	public void _GameOver()
    {
        Debug.Log(this.gameObject.name);
        AI_Enemy.activeEnemies = 0;

        fadeObj.SetActive(true);
        anim.SetTrigger("Fade");
        StartCoroutine("WaitFade");
        typing.SetActive(false);

    }

    IEnumerator WaitFade()
    {
        Debug.Log("Waiting");
        completionText.text = "";
        lettersPerSecondText.text = "";
        nextLevelBtn.SetActive(false);

        while (anim.GetCurrentAnimatorStateInfo(0).IsName("anim_FadeOut"))
        {
            yield return null;
        }
        Debug.Log("Done");
        completionText.text = "MISSION FAILED!";
        lettersPerSecondText.text = "Record Letters Per Second: " + Typing.recordLettersPerSecond;
        restartLevelBtn.SetActive(true);
    }
}
