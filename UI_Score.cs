﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_Score : MonoBehaviour {

    public float displayScore = 0;
    Text scoreText = null;
    public float catchupSpeed = 1f;

    public UnityEvent OnScoreChange;

    void Awake()
    {
        scoreText = GetComponent<Text>();
    }

    void Update()
    {
        //Debug.Log("Game Manager Score: " + GameManager.Instance.score);
        displayScore = Mathf.Lerp(displayScore, GameManager.Instance.score, catchupSpeed);
        scoreText.text = "Score " + Mathf.CeilToInt(displayScore).ToString("D6");
    }
}
