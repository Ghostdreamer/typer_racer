﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MouseOrbitImproved : MonoBehaviour
{
    public bool ran = false;
    void Awake()
    {
        if (!ran)
        {
            transform.Rotate(transform.rotation.x, transform.rotation.y, Random.Range(0.0f, 360.0f));
            ran = true;
        }
        else
            DestroyImmediate(this);
    }
}
