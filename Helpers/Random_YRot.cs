﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class Random_YRot : MonoBehaviour
{

    public bool ran = false;


    void Awake()
    {
        if (!ran)
        {
            transform.Rotate(0f, Random.Range(0.0f, 360.0f), 0f);
            ran = true;
        }
        else
            DestroyImmediate(this);
    }


}
