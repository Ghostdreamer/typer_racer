﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Health : MonoBehaviour {

    Health playerHealth = null;
    Image healthImg;

    void Awake()
    {
        playerHealth = GameObject.FindGameObjectWithTag("PlayerChar").GetComponent<Health>();
        healthImg = GetComponent<Image>();
    }

    public void _UpdateHealth()
    {
        healthImg.fillAmount = playerHealth.Amount / 100f;
    }

    public void _DisableEnemies()
    {
        AI_Enemy[] enemies = Object.FindObjectsOfType<AI_Enemy>();
        foreach (AI_Enemy enemy in enemies)
        {
            enemy.ActiveState = AI_Enemy.AI_STATE.Idle;
        }

        Debug.Log("Disabling");
    }
}
