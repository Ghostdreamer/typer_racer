﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_General : MonoBehaviour
{

    public void _LoadFirst()
    {
        if (SaveLoad.Load_HasPlayed() == 1)
            SceneManager.LoadScene("Level1");
        else
        {
            SaveLoad.Save_HasPlayed();
            SceneManager.LoadScene("Menu_Help");
        }
    }
	public void _LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);

    }

    public void _Quit()
    {
        Application.Quit();
    }
}
